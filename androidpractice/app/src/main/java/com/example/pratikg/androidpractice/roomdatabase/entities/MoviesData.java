package com.example.pratikg.androidpractice.roomdatabase.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

/**
 * Created by pratikg on 2/8/18.
 */
@Entity(tableName = "movies", primaryKeys = {"title"})
public class MoviesData {
    @NonNull
    @ColumnInfo(name = "title")
    public String title;


    @ColumnInfo(name = "poularity")
    public String popularity;

    @NonNull
    @ColumnInfo(name = "votecount")
    public String votecount;

    @ColumnInfo(name = "releasedate")
    public String releasedate;


}
