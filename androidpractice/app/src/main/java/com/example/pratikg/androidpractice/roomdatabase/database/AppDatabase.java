package com.example.pratikg.androidpractice.roomdatabase.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.pratikg.androidpractice.roomdatabase.dao.CustomerDao;
import com.example.pratikg.androidpractice.roomdatabase.entities.MoviesData;


/**
 * Created by pratikg on 2/8/18.
 */
@Database(entities = {MoviesData.class},version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;
    public abstract CustomerDao customerDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class,
                    "test-db")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}
