package com.example.pratikg.androidpractice.roomdatabase.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.pratikg.androidpractice.roomdatabase.entities.MoviesData;


/**
 * Created by pratikg on 2/8/18.
 */
@Dao
public interface CustomerDao {
    @Insert
    void insert(MoviesData... moviesData);

    @Update
    void update(MoviesData... moviesData);

    @Delete
    void delete(MoviesData... moviesData);

    @Query("Select * FROM movies")
    MoviesData loadCustomerData();

    @Query("DELETE FROM movies")
    void delete();

}
