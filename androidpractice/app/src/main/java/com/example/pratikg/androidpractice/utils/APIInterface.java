package com.example.pratikg.androidpractice.utils;

import com.example.pratikg.androidpractice.objects.ObjMoviesRes;

import retrofit2.Call;
import retrofit2.http.GET;


public interface APIInterface {

    @GET("movie?api_key=ef93afc9d46b623afdad566f9c34c471&primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22?")
    Call<ObjMoviesRes> doGetListResources();



}
