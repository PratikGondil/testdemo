package com.example.pratikg.androidpractice.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pratikg.androidpractice.R;
import com.example.pratikg.androidpractice.interfaces.OnClickItemListner;
import com.example.pratikg.androidpractice.objects.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MoviesRecycleAdapter extends RecyclerView.Adapter<MoviesRecycleAdapter.RecyclerViewHolders> {

    private List<Result> itemList;
    private Context context;
    String packageName;
    Fragment fragment;
    List<Integer> postionArray = new ArrayList<>();
    OnClickItemListner onClickItemListner;


    public MoviesRecycleAdapter(Context context, List<Result> itemList, Fragment fragment, OnClickItemListner onClickItemListener) {
        this.itemList = itemList;
        this.context = context;
        this.fragment = fragment;
        this.onClickItemListner = onClickItemListener;
        packageName = context.getPackageName();
    }


    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_card, null);

        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {

        holder.txtMovieName.setText(itemList.get(position).getTitle());
        holder.txtMoviePopularity.setText(String.valueOf(itemList.get(position).getPopularity()));
        holder.txtMovieVoteCount.setText(String.valueOf(itemList.get(position).getVoteCount()));
        holder.txtReleaseDate.setText(String.valueOf(itemList.get(position).getReleaseDate()));

        Picasso.get().load("https://image.tmdb.org/t/p/w500/" + itemList.get(position).getPosterPath()).into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView txtMovieName;
        private TextView txtMovieVoteCount;
        private TextView txtMoviePopularity;
        private TextView txtReleaseDate;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            txtMovieName = (TextView) itemView.findViewById(R.id.txtMovieName);
            txtMovieVoteCount = (TextView) itemView.findViewById(R.id.txtMovieVoteCount);
            txtMoviePopularity = (TextView) itemView.findViewById(R.id.txtMoviePopularity);
            txtReleaseDate =(TextView)itemView.findViewById(R.id.txtReleaseDate);



        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}