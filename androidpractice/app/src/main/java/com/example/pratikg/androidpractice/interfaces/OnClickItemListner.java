package com.example.pratikg.androidpractice.interfaces;

import com.example.pratikg.androidpractice.adapters.MoviesRecycleAdapter;

import java.util.List;

public interface OnClickItemListner {

    void onClickItem(MoviesRecycleAdapter.RecyclerViewHolders holder, List<Integer> postionArray, int position);

}
