package com.example.pratikg.androidpractice.roomdatabase.entities;

import android.arch.persistence.room.TypeConverter;

import com.example.pratikg.androidpractice.objects.ObjMoviesRes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Converters {
    @TypeConverter
    public static ArrayList<ObjMoviesRes> fromTimestamp(String value) {
        Type listType = new TypeToken<ArrayList<ObjMoviesRes>>() {}.getType();
        return new Gson().fromJson(value, listType);
        // return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static String arraylistToString(ArrayList<ObjMoviesRes> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        return json;
        // return date == null ? null : date.getTime();
    }
}
