package com.example.pratikg.androidpractice.roomdatabase.database;

import android.content.Context;

import com.example.pratikg.androidpractice.objects.ObjMoviesRes;
import com.example.pratikg.androidpractice.roomdatabase.entities.MoviesData;


/**
 * Created by pratikg on 2/8/18.
 */

public class DataOperations {
    private static DataOperations instance;
    private static AppDatabase dataBase;


    public static DataOperations with(AppDatabase appDataBase) {

        if (dataBase == null)
            dataBase = appDataBase;

        if (instance == null)
            instance = new DataOperations();

        return instance;
    }


    public void addCustomerData(ObjMoviesRes objMoviesRes) {
        if (dataBase == null)
            return;

        MoviesData moviesData = new MoviesData();
        if (objMoviesRes != null) {
            moviesData = customerDataInstance(objMoviesRes);
        }
        dataBase.customerDao().insert(moviesData);

        dataBase.customerDao().loadCustomerData();
    }



    private MoviesData customerDataInstance(ObjMoviesRes objMoviesRes) {
        MoviesData customerData = new MoviesData();
        if (objMoviesRes.getResults() != null) {

            for (int i = 0; i <objMoviesRes.getResults().size() ; i++) {
              customerData.title=objMoviesRes.getResults().get(i).getTitle();
              customerData.popularity =String.valueOf(objMoviesRes.getResults().get(i).getPopularity());
              customerData.votecount =String.valueOf(objMoviesRes.getResults().get(i).getVoteCount());
              customerData.releasedate =String.valueOf(objMoviesRes.getResults().get(i).getReleaseDate());
            }


        }
        return customerData;
    }





}
