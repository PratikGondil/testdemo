package com.example.pratikg.androidpractice.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pratikg.androidpractice.R;
import com.example.pratikg.androidpractice.adapters.MoviesRecycleAdapter;
import com.example.pratikg.androidpractice.baseactivity.MasterFragment;
import com.example.pratikg.androidpractice.interfaces.OnClickItemListner;
import com.example.pratikg.androidpractice.objects.ObjMoviesRes;
import com.example.pratikg.androidpractice.roomdatabase.database.AppDatabase;
import com.example.pratikg.androidpractice.roomdatabase.database.DataOperations;
import com.example.pratikg.androidpractice.roomdatabase.entities.MoviesData;
import com.example.pratikg.androidpractice.utils.APIClient;
import com.example.pratikg.androidpractice.utils.APIInterface;
import com.example.pratikg.androidpractice.utils.SimpleDividerItemDecoration;
import com.example.pratikg.androidpractice.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MoviesListFragment extends MasterFragment implements OnClickItemListner {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView recyclerView;
    private String mParam1;
    private String mParam2;
    View view;
    APIInterface apiInterface;
    private LinearLayoutManager lLayout;
    MoviesRecycleAdapter adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    AppDatabase appDatabase;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_test, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Movies");

        appDatabase = Utils.getDatabase(getActivity());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        initApiCAll();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                shuffle();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }



    public void shuffle(){
       initApiCAll();
    }

    /*
        Calling the Api here
     */
    private void initApiCAll() {
        showProgress(getActivity());

        Call<ObjMoviesRes> call = apiInterface.doGetListResources();
        call.enqueue(new Callback<ObjMoviesRes>() {
            @Override
            public void onResponse(Call<ObjMoviesRes> call, Response<ObjMoviesRes> response) {
                stopProgress();

                ObjMoviesRes objMoviesRes = response.body();
                if(appDatabase!=null)
                {
                    MoviesData moviesData=appDatabase.customerDao().loadCustomerData();

                    if(moviesData==null)
                    {
                        DataOperations.with(appDatabase).addCustomerData(objMoviesRes);

                    }

                }



                if(!objMoviesRes.getResults().isEmpty()) {
                    Log.v("Response", response.toString());
                    recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                    lLayout = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(lLayout);
                     adapter = new MoviesRecycleAdapter(getActivity(), objMoviesRes.getResults(), MoviesListFragment.this,MoviesListFragment.this);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                }

            }

            @Override
            public void onFailure(Call<ObjMoviesRes> call, Throwable t) {

            }


        });


    }


    @Override
    public void onClickItem(MoviesRecycleAdapter.RecyclerViewHolders holder, List<Integer> postionArray, int position) {


    }


}
